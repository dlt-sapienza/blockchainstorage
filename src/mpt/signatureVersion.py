#sample_smart_sig.py
from algosdk.atomic_transaction_composer import *
from pyteal import *

position = Seq(

)
@Subroutine(TealType.uint64)
def prova(prova: Expr) -> Expr:
    return prova == Bytes("prova")

"""Basic Donation Escrow"""
def verifyProof(benefactor):
    Fee = Int(1000)
    i = ScratchVar(TealType.uint64)
    len = ScratchVar(TealType.uint64)
    path = ScratchVar(TealType.bytes)

    rootHash = ImportScratchValue(1, 20)  # loading scratch slot 20 from the transaction at index 0

    #Only the benefactor account can withdraw from this escrow
    program = Seq(
        And( # Precondition
            Gtxn[2].type_enum() == TxnType.Payment,
            Txn.receiver() == Addr(benefactor),
            Global.group_size() == Int(3),
        ),

        len.store(Btoi(Gtxn[2].application_args[0])),

        path.store(Concat(Gtxn[2].receiver(), Gtxn[0].config_asset_unit_name())),
        prova(Gtxn[2].application_args[0]),
        # For(
        #     i.store(Int(1)), i.load() < len.load(), i.store(i.load() + Int(1))
        # ).Do(
        #     #Btoi(GetByte(Gtxn[2].application_args[i.load()], Int(0)))
        #     prova(Gtxn[2].application_args[i.load()])
        # )
        Approve()
    )
    # Mode.Signature specifies that this is a smart signature
    return compileTeal(program, Mode.Signature, version=5)

test_benefactor = "CZHGG36RBYTTK36N3ZC7MENGFOL3R6D4NNEJQU3G43U5GH457SU34ZGRLY"
print( verifyProof(test_benefactor))