from typing import Literal as L
from algosdk.atomic_transaction_composer import *
from pyteal import *

merkle_root = Bytes("root")
main_app = Bytes("MainContract")
library_app = Bytes("LibraryContract")
HASH_NULL_NODE = Bytes("940187241D938788863DA826AAC00199002F9609C86468DE5CC22BF3D478881E")



@Subroutine(TealType.uint64)
def position(elem: Expr) -> Expr:
    return Seq(
        Cond(
            [elem == Bytes("A"), Return(0)],
            [elem == Bytes("B"), Return(1)],
            [elem == Bytes("C"), Return(2)],
            [elem == Bytes("D"), Return(3)],
            [elem == Bytes("E"), Return(4)],
            [elem == Bytes("F"), Return(5)],
            [elem == Bytes("G"), Return(6)],
            [elem == Bytes("H"), Return(7)],
            [elem == Bytes("I"), Return(8)],
            [elem == Bytes("J"), Return(9)],
            [elem == Bytes("K"), Return(10)],
            [elem == Bytes("L"), Return(11)],
            [elem == Bytes("M"), Return(12)],
            [elem == Bytes("N"), Return(13)],
            [elem == Bytes("O"), Return(14)],
            [elem == Bytes("P"), Return(15)],
            [elem == Bytes("Q"), Return(16)],
            [elem == Bytes("R"), Return(17)],
            [elem == Bytes("S"), Return(18)],
            [elem == Bytes("T"), Return(19)],
            [elem == Bytes("U"), Return(20)],
            [elem == Bytes("V"), Return(21)],
            [elem == Bytes("W"), Return(22)],
            [elem == Bytes("X"), Return(23)],
            [elem == Bytes("Y"), Return(24)],
            [elem == Bytes("Z"), Return(25)],
            [elem == Bytes("1"), Return(26)],
            [elem == Bytes("2"), Return(27)],
            [elem == Bytes("3"), Return(28)],
            [elem == Bytes("4"), Return(29)],
            [elem == Bytes("5"), Return(30)],
            [elem == Bytes("6"), Return(31)],
            [elem == Bytes("7"), Return(32)],
            [elem == Bytes("8"), Return(33)],
            [elem == Bytes("9"), Return(34)],
            [elem == Bytes("0"), Return(35)],
        )
    )

def getVerifierContract():
    # Main router class
    router = Router(
        # Name of the contract
        "VerifyContract",
        # What to do for each on-complete type when no arguments are passed (bare call)
        BareCallActions(
            # On create only, just approve
            # no_op=OnCompleteAction.create_only(Approve()),
            # Always let creator update/delete but only by the creator of this contract
            # update_application=OnCompleteAction.always(Reject()),
            # delete_application=OnCompleteAction.call_only(on_delete),
        ),
    )


    @router.method(no_op=CallConfig.CREATE)
    def create_app(main: abi.Uint16, library: abi.Uint16) -> Expr:

        return Seq(
            App.globalPut(merkle_root, Bytes("B3343239D739F3971E297E798C9D28AD69EA8AA9612F998C03A4F1921E3DC2DA")),
            App.globalPut(main_app, main),
            App.globalPut(library_app, library),
        )

    @router.method(no_op=CallConfig.CALL)
    def computeSha256(proof: abi.StaticArray[abi.String, L[37]]):
        computed_hash = ScratchVar(TealType.bytes, 21)
        return Seq(
                computed_hash.store(Sha256(proof)), Approve()
            )
                    

    return router

getVerifierContract()