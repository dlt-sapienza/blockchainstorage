from typing import Literal as L
from algosdk.atomic_transaction_composer import *

# user declared account mnemonics
creator_mnemonic = "employ spot view century canyon fossil upon hollow tone chicken behave bamboo cool correct vehicle mirror movie scrap budget join music then poverty ability gadget"
# user declared algod connection parameters. Node must have EnableDeveloperAPI set to true in its config
algod_address = "http://localhost:4001"
algod_token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

from pyteal import *

merkle_root = Bytes("root")
verify_app = Bytes("VerifyContract")
library_app = Bytes("LibraryContract")

@Subroutine(TealType.bytes)
def buildKey(assetID: Expr, account: Expr) -> Expr:
    return Seq(
        Return(Concat(account, assetID)),
    )

def getMainContract():
    # Main router class
    router = Router(
        # Name of the contract
        "MainContract",
        # What to do for each on-complete type when no arguments are passed (bare call)
        BareCallActions(
            # On create only, just approve
            # no_op=OnCompleteAction.create_only(Approve()),
            # Always let creator update/delete but only by the creator of this contract
            # update_application=OnCompleteAction.always(Reject()),
            # delete_application=OnCompleteAction.call_only(on_delete),
        ),
    )


    @router.method(no_op=CallConfig.CREATE)
    def create_app(verifier: abi.Uint16) -> Expr:

        return Seq(
            App.globalPut(merkle_root, Bytes("B3343239D739F3971E297E798C9D28AD69EA8AA9612F998C03A4F1921E3DC2DA")),
            App.globalPut(verify_app, verifier),
        )

    @router.method(no_op=CallConfig.CALL)
    def verify_and_create(proof: abi.DynamicArray[abi.StaticArray[abi.String, L[37]]], root: abi.StaticBytes[L[32]], assetID: abi.String):
        key = ScratchVar(TealType.bytes, 20)
        return Seq(
            # 1. build the key
            key.store(buildKey(assetID, Txn.sender())),
            # 2. and 3. verifyProof and update the Tree
            InnerTxn.Begin(),
            InnerTxn.SetFields({
                TxnField.type_enum: TxnType.ApplicationCall,
                TxnField.application_id: App.globalGet(verify_app),
                TxnField.application_args: [Bytes("verify"), proof, key.load(), root]
            }),
            InnerTxn.Submit(),

            # 3. retrieve the new root hash
            If (App.globalGetEx(App.globalGet(verify_app), root).hasValue()).Then(
                App.globalPut(root, App.globalGetEx(App.globalGet(verify_app), root).value()),

                # 4.Create the asset
                InnerTxn.Begin(),
                InnerTxn.SetFields(
                    {
                        TxnField.type_enum: TxnType.AssetConfig,
                        TxnField.config_asset_name: assetID,
                        TxnField.config_asset_total: Int(10000),
                        TxnField.config_asset_decimals: Int(2),
                        TxnField.config_asset_clawback: Txn.sender(),
                        TxnField.config_asset_manager: Txn.sender(),
                        TxnField.config_asset_reserve: Txn.sender(),
                        TxnField.config_asset_freeze: Txn.sender()
                    }
                ),
                InnerTxn.Submit()
            ),

            
        )

    return router

getMainContract()