import hashlib

from src.util import *

creator_mnemonic = "employ spot view century canyon fossil upon hollow tone chicken behave bamboo cool correct vehicle mirror movie scrap budget join music then poverty ability gadget"

NULL = None
NULL_NODE = [NULL, 'False']

_hash = lambda h: hashlib.sha256(h).hexdigest().upper()
to_bytes = lambda node: bytes("{0}".format(node), 'utf-8')

HASH_NULL_NODE = _hash(to_bytes(NULL_NODE))

empty_list = lambda: [HASH_NULL_NODE for el in range(37)]
empty_leaf = lambda: [NULL for el in range(2)]

rootNode: list = empty_list()
root = _hash(to_bytes(rootNode))
db = {root: rootNode}

def get_node(_db: dict, el) -> list:
    if isinstance(el, str): return _db[el]
    h = _hash(to_bytes(el))
    if h in _db.keys():
        return _db[_hash(to_bytes(el))]
    return None

def not_null_elem(node: list):
    res = []
    for el in node:
        if el != HASH_NULL_NODE:
            res.append(el)
    return res


def _position(el: chr):
    n = ord(el)
    if (n >=65 and n <= 90):
        return n - 65
    elif (n >= 48 and n <= 57):
        return (n - 48) + 26
    return NULL


def add(path: str, initial_path: str, node: list = None):
    global root, rootNode
    if path == '': # Base case
        curnode = get_node(db, node) if node != HASH_NULL_NODE else empty_leaf()
        curnode[-1] = "True"
    else: # Recursive case
        curnode = get_node(db, node) if node != HASH_NULL_NODE else empty_list()
        newindex = add(path[1:], initial_path, curnode[_position(path[0])])
        curnode[_position(path[0])] = newindex
    db[_hash(to_bytes(curnode))] = curnode

    if path == initial_path: # update the root pointer
        temp = root
        root = _hash(to_bytes(curnode))
        rootNode = curnode
        del db[temp]
    return _hash(to_bytes(curnode))


def add_v2(path:str, initial_path: str, proof:list, i = 0):
    global root, rootNode
    if path == '': # Base case
        curnode = empty_leaf()
        curnode[-1] = "True"
    else: # Recursive case
        curnode = proof[0] if proof and proof[0] != NULL_NODE else empty_list()
        newindex = add_v2(path[1:], initial_path, proof[1:], i+1) if proof and len(proof) > 1 else add_v2(path[1:], initial_path, None, i+1)
        curnode[_position(path[0])] = newindex
    db[_hash(to_bytes(curnode))] = curnode # NOT IN SMART CONTRACT

    if path == initial_path: # update the root pointer
        temp = root
        root = _hash(to_bytes(curnode))
        rootNode = curnode
        del db[temp]
    return _hash(to_bytes(curnode))

def delete_helper(path: str, node: list):
    if node is NULL:
        return NULL
    else:
        curnode = get_node(db, node)
        old_hash = _hash(to_bytes(curnode))
        if path == "":
            curnode = NULL_NODE
        else:
            (newindex, _) = delete_helper(path[1:], db[curnode[_position(path[0])]])
            curnode[_position(path[0])] = newindex

        if len(not_null_elem(curnode)) == 0:
            del db[old_hash]
            return HASH_NULL_NODE, curnode
        else:
            db[_hash(to_bytes(curnode))] = curnode
            return _hash(to_bytes(curnode)), curnode


def delete(path: str, node: list):
    global root, rootNode
    (h, new_root) = delete_helper(path, node)

    temp = root
    if len(not_null_elem(new_root)) == 0:
        rootNode = empty_list()
        root = _hash(to_bytes(rootNode))
        db[root] = rootNode
        return root
    root = _hash(to_bytes(new_root))
    rootNode = new_root
    del db[temp]
    return h

def delete_v2(path:str, node:list):
    pass


def get_proof(path: str, node: list, proof=None):
    empty = empty_list()
    if node == empty:
        return [NULL, 'False']
    if proof is None:
        proof = []
    curnode = get_node(db, node)
    if curnode == NULL:
        return [NULL, 'False']
    if path == "":
        if len(node) != 2:
            proof.append([NULL, 'False'])
        else:
            proof.append(curnode)
            if curnode[-1] != 'True':
                proof.append([NULL, 'False'])
    else:
        proof.append(curnode)
        if curnode[_position(path[0])] != HASH_NULL_NODE:

            get_proof(path[1:], get_node(db, curnode[_position(path[0])]), proof)
        else:
            proof.append([NULL, 'False'])
    return proof

# verify Proof with access to db
def verify_proof(path:str, node: list, proof: list) -> bool:
    if proof is None or proof == [] or proof == [NULL, 'False']:
        return False
    curnode = get_node(db, node)
    if path == "":
        if len(node) == 2 and node[1] == 'True':
            return True
        return False
    else:
        child = get_node(db, curnode[_position(path[0])])
        return verify_proof(path[1:], child, proof[1:]) and _hash(to_bytes(child)) == proof[0][_position(path[0])]

# Verify not inclusion of a node
def verify_proof_v2(path:str, proof: list) -> bool:
    h = _hash(to_bytes(proof[0]))
    # if len(proof) != len(path)+1: raise Exception("Proof length and path one differs. The non inclusion proof is not valid")
    if proof is None or proof == [] or proof == [NULL, 'False'] or h != root:
        return False
    for i in range(len(proof)-1):
        if proof[i][_position(path[i])] != _hash(to_bytes(proof[i+1])):
            return proof[i][_position(path[i])] != HASH_NULL_NODE
    return True

def verify_non_inclusion(path:str, proof: list) -> bool:
    return verify_proof_v2(path, proof) and proof[-1] == NULL_NODE

def verify_and_add(path:str):
    global rootNode

    proof = get_proof(path, rootNode)  # The only that can access to db

    res = verify_non_inclusion(path, proof)  # No access to the DB
    if res:
        add_v2(path, path, proof)  # No access to the DB
        return True

    return False

def verify_and_delete(path:str):
    global rootNode

    proof = get_proof(path, rootNode)  # The only that can access to db

    res = verify_non_inclusion(path, proof)  # No access to the DB
    if not res:
        # TODO
        delete_v2(path, proof)  # No access to the DB
        return True

    return False

if __name__ == "__main__":
    # define sender
    sender = account.address_from_private_key(mnemonic.to_private_key(creator_mnemonic))
    print(sender)
    print(HASH_NULL_NODE)

    add(sender, sender, rootNode)
    add("PROVA", "PROVA", rootNode)

    for i in range(100):
        (_, sender2) = account.generate_account()
        print("Is the {0} added to tree? {1}".format(sender2, verify_and_add(sender2)))

    proof = get_proof(sender, rootNode)
    print("Is {0} not included? {1}".format(sender, verify_non_inclusion(sender, proof)))
    proof = get_proof("PROV", rootNode)
    print("Is {0} not included? {1}".format("PROV", verify_non_inclusion("PROV", proof)))
    delete(sender, rootNode)
    try:
        proof = get_proof(sender, rootNode)
        print("Is {0} not included? {1}".format(sender, verify_non_inclusion(sender, proof)))
    except:
        print("Error in getting {0}".format(sender))

    print("Is the {0} added to tree? {1}".format(sender, verify_and_add(sender)))

    proof = get_proof(sender, rootNode)
    print("Is {0} not included? {1}".format(sender, verify_non_inclusion(sender, proof)))

    print("Is the {0} added to tree? {1}".format(sender, verify_and_add(sender)))

