from src.util import *
from algosdk.v2client import algod
from algosdk.logic import get_application_address
from src.local_storage.local_storage_contract import *

# user declared account mnemonics
creator_mnemonic = "employ spot view century canyon fossil upon hollow tone chicken behave bamboo cool correct vehicle mirror movie scrap budget join music then poverty ability gadget"
# user declared algod connection parameters. Node must have EnableDeveloperAPI set to true in its config
algod_address = "https://testnet-algorand.api.purestake.io/ps2"
algod_token = "F9I45UDrFb9FuoYhohD35A2Tcfq0mnZ5cTuG81x9"
headers = {
    "X-API-Key": algod_token,
}

def createRight(
        client: algod.AlgodClient,
        app_id: int,
        account_sk: str,
        right: str
):
    app_addr = get_application_address(app_id)

    suggestedParams = client.suggested_params()

    atc = AtomicTransactionComposer()
    rh_addr = account.address_from_private_key(account_sk)
    signer_rh = AccountTransactionSigner(account_sk)

    app_args = [
        right
    ]

    with open("./auction_contract.json") as f:
        js = f.read()

    atc.add_method_call(app_id=app_id, method=get_method('verify_and_create', js), sender=rh_addr,
                        sp=suggestedParams, signer=signer_rh, method_args=app_args)

    result = atc.execute(client, 10)

    tx_confirmed = transaction.wait_for_confirmation(client, result.tx_ids[0])
    print("Local state:", read_local_state(client, rh_addr, app_id))
    return tx_confirmed['inner-txns'][0]['asset-index']

def optinContract(
        client: algod.AlgodClient,
        app_id: str,
        senderSK: str
):
    atc = AtomicTransactionComposer()
    rh_addr = account.address_from_private_key(senderSK)
    signer_rh = AccountTransactionSigner(senderSK)
    suggestedParams = client.suggested_params()

    with open("./auction_contract.json") as f:
        js = f.read()

    atc.add_method_call(app_id=app_id, method=get_method('optin', js), sender=rh_addr,
                        on_complete=transaction.OnComplete.OptInOC,
                        sp=suggestedParams, signer=signer_rh)

    result = atc.execute(client, 10)
    transaction.wait_for_confirmation(client, result.tx_ids[0])
    print("Local state:", read_local_state(client, rh_addr, app_id))
def createLocalStorageApp(
        algod_client: algod.AlgodClient,
        senderSK: str,
    ):
    # declare application state storage (immutable)
    local_ints = 0
    local_bytes = 1
    global_ints = 0
    global_bytes = 0
    global_schema = transaction.StateSchema(global_ints, global_bytes)
    local_schema = transaction.StateSchema(local_ints, local_bytes)

    # Compile the program
    router = getLocalStorageVersionContract()
    approval_program, clear_program, contract = router.compile_program(version=6,
                                                                       optimize=OptimizeOptions(scratch_slots=True))

    with open("./auction_approval.teal", "w") as f:
        f.write(approval_program)

    with open("./auction_clear.teal", "w") as f:
        f.write(clear_program)

    with open("./auction_contract.json", "w") as f:
        import json

        f.write(json.dumps(contract.dictify()))

    # compile program to binary
    approval_program_compiled = compile_program(algod_client, approval_program)

    # compile program to binary
    clear_state_program_compiled = compile_program(algod_client, clear_program)

    print("--------------------------------------------")
    print("Deploying Auction application......")
    print("--------------------------------------------")


    app_id = create_app(
        algod_client,
        senderSK,
        approval_program_compiled,
        clear_state_program_compiled,
        global_schema,
        local_schema,
    )
    print("Global state:", read_global_state(algod_client, app_id))

    assert app_id is not None and app_id > 0
    return app_id, contract
def main():
    creator_private_key = get_private_key_from_mnemonic(creator_mnemonic)
    creator_address = account.address_from_private_key(creator_private_key)
    siae_account = account.generate_account()
    print(creator_address)

    # initialize an algodClient
    algod_client = algod.AlgodClient(algod_token, algod_address, headers)

    app_id, contract = createLocalStorageApp(algod_client, creator_private_key)

    print("--------------------------------------------")
    print("Sending funding to the contract and the CMO......")
    print("--------------------------------------------")

    txn = transaction.PaymentTxn(
        sender=creator_address,
        receiver=get_application_address(app_id),
        amt=1000000,
        sp=algod_client.suggested_params(),
    )
    signed_txn = txn.sign(creator_private_key)
    algod_client.send_transaction(signed_txn)
    transaction.wait_for_confirmation(algod_client, signed_txn.get_txid())

    txn = transaction.PaymentTxn(
        sender=creator_address,
        receiver=siae_account[1],
        amt=1000000,
        sp=algod_client.suggested_params(),
    )
    signed_txn = txn.sign(creator_private_key)
    algod_client.send_transaction(signed_txn)
    transaction.wait_for_confirmation(algod_client, signed_txn.get_txid())

    print("--------------------------------------------")
    print("Rightsholder optin the SC......")
    print("--------------------------------------------")
    optinContract(algod_client, app_id, creator_private_key)

    print("--------------------------------------------")
    print("Creating the right ITTV......")
    print("--------------------------------------------")
    nft_id = createRight(algod_client, app_id, creator_private_key, "ITTV")

    print("--------------------------------------------")
    print("Creating the right DETV......")
    print("--------------------------------------------")
    createRight(algod_client, app_id, creator_private_key, "DETV")
    try:
        print("--------------------------------------------")
        print("Trying to recreate the right ITTV......")
        print("--------------------------------------------")
        createRight(algod_client, app_id, creator_private_key, "ITTV")
    except:
        print("Error in generating ITTV")

    app_addr = get_application_address(app_id)

    print("--------------------------------------------")
    print("Enabling CMO to receive the asset......")
    print("--------------------------------------------")

    atxn = transaction.AssetTransferTxn(siae_account[1], algod_client.suggested_params(), siae_account[1], 0, nft_id)
    signed_txn = atxn.sign(siae_account[0])
    algod_client.send_transaction(signed_txn)
    transaction.wait_for_confirmation(algod_client, signed_txn.get_txid())

    print("--------------------------------------------")
    print("Transfering the asset to the CMO......")
    print("--------------------------------------------")

    atxn = transaction.AssetTransferTxn(creator_address, algod_client.suggested_params(), siae_account[1], 10000, nft_id, None, app_addr)
    signed_txn = atxn.sign(creator_private_key)
    algod_client.send_transaction(signed_txn)
    transaction.wait_for_confirmation(algod_client, signed_txn.get_txid())

main()