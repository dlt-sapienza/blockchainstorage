from pyteal import *

territory_len = Int(5)
right_len = Int(2)
table_key = Bytes("table") # MAX 128 bytes the sum between key and value
empty_str = "".join("-"*123)

@Subroutine(TealType.uint64)
def positionTerritory(elem: Expr) -> Expr:
    return Cond(
        [elem == Bytes("DE"), Return(Int(0))],
        [elem == Bytes("FR"), Return(Int(1))],
        [elem == Bytes("GB"), Return(Int(2))],
        [elem == Bytes("IT"), Return(Int(3))],
        [elem == Bytes("SP"), Return(Int(4))],
        [Int(1), Return(territory_len)]
    )

@Subroutine(TealType.uint64)
def positionRight(elem: Expr) -> Expr:
    return Cond(
        [elem == Bytes("ON"), Return(Int(0))],
        [elem == Bytes("TV"), Return(Int(1))],
    )

@Subroutine(TealType.uint64)
def positionRight(elem: Expr) -> Expr:
    return Cond(
        [elem == Bytes("ON"), Return(Int(0))],
        [elem == Bytes("TV"), Return(Int(1))],
        [Int(1), Return(right_len)]
    )

@Subroutine(TealType.none)
def createAsset(assetID: Expr) -> Expr:
    return Seq(# create the asset
        InnerTxnBuilder.Begin(),
        InnerTxnBuilder.SetFields(
            {
                TxnField.type_enum: TxnType.AssetConfig,
                TxnField.config_asset_name: assetID,
                TxnField.config_asset_total: Int(10000),
                TxnField.config_asset_decimals: Int(2),
                TxnField.config_asset_clawback: Txn.sender(),
                TxnField.config_asset_manager: Txn.sender(),
                TxnField.config_asset_reserve: Txn.sender(),
                TxnField.config_asset_freeze: Txn.sender()
            }
        ),
        InnerTxnBuilder.Submit()
)

def getLocalStorageVersionContract():
    # Main router class
    router = Router(
        # Name of the contract
        "LocalStorageVersionContract",
        # What to do for each on-complete type when no arguments are passed (bare call)
        BareCallActions(
            # On create only, just approve
            no_op=OnCompleteAction.create_only(Approve()),
            # Always let creator update/delete but only by the creator of this contract
            update_application=OnCompleteAction.always(Reject()),
            # delete_application=OnCompleteAction.call_only(on_delete),
        ),
    )


    @router.method(no_op=CallConfig.CALL)
    def verify_and_create(assetID: abi.String):
        right_index = ScratchVar(TealType.uint64)
        terr_index = ScratchVar(TealType.uint64)
        table = ScratchVar(TealType.bytes)
        new_table = ScratchVar(TealType.bytes)
        table_index = ScratchVar(TealType.uint64)
        return Seq(
            Assert(Len(assetID.get()) == Int(4)),
            right_index.store(positionTerritory(Extract(assetID.get(), Int(0), Int(2)))),
            terr_index.store(positionRight(Extract(assetID.get(), Int(2), Int(2)))),
            table.store(App.localGet(Txn.sender(), table_key)),
            table_index.store(Add(Mul(right_index.load(), territory_len), terr_index.load())),
            If(
                GetBit(table.load(), table_index.load()) == Int(0)
            )
            .Then(
                Seq(
                    new_table.store(SetBit(table.load(), table_index.load(), Int(1))),
                    App.localPut(Txn.sender(), table_key, new_table.load()),
                    createAsset(assetID.get()),
                    Approve()
                )
            ).Else(
                Reject()
            )
        )
    
    @router.method(opt_in=CallConfig.CALL)
    def optin():
        return Seq(
            App.localPut(Txn.sender(), table_key, Bytes(bytearray(123)) )
        )

    return router